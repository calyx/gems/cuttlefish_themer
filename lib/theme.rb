require "bootstrap"

require "zeitwerk"
loader = Zeitwerk::Loader.for_gem
loader.setup

module Theme
  def self.configure
    yield config
  end

  def self.config
    @config ||= Configuration.new
  end
end

# loader.eager_load # optionally
