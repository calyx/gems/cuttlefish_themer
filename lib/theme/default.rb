module Theme::Default
  VARS = {
    favicon: "/assets/theme/favicon.png",

    ##
    ## CONTENT
    ##
    content_bg: "#FFF",

    ##
    ## HEADER
    ##
    header_text: "New Site",
    header_bg: "#AE99D1",
    header_fg: "#FFF",
    header_icon_bg: "#9679C3",
    header_icon_url: "/assets/theme/site-icon.svg",
    header_icon_position: "center center",
    header_image_url: nil,
    header_image_position: "left center",

    ##
    ## SIDEBAR
    ##
    sidebar_active_highlight: "var(--header-bg)"

  }
end